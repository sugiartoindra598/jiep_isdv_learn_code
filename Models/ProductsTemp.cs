﻿using System;
using System.Collections.Generic;

namespace LearCore.Models
{
    public partial class ProductsTemp
    {
        public double? ProductId { get; set; }
        public string ProductName { get; set; }
        public double? SupplierId { get; set; }
        public double? CategoryId { get; set; }
        public string QuantityPerUnit { get; set; }
        public double? UnitPrice { get; set; }
        public double? UnitsInStock { get; set; }
    }
}
